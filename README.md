# elastalert-container-build
Build Yelp/Elastalert into a docker container. This version only relates to v7.6.2 of elasticsearch

Tested against v0.2.4 of Yelp/Elastalert which now uses Python 3.6 against Elasticsearch:7.6.2

Dockerfile cribbed from jertel/elastalert-docker

## Build from docker-compose
```
cd <elastalert-docker>
docker-compose build # will create an image called elastalert_image
docker-compose up -d # will create a container called elastalert_container

docker tag elastalert_image elastalert_image:7.6.2

# to push to dockerhub
docker tag elastalert_image <dockerHubUser>/elastalert_image:7.6.2
docker push <dockerHubUser>/elastalert_image:7.6.2
```

In this repo is a directory called `docker_config_volume` which contains example config. Present this directory as `/opt/config` when running the container and change the contents accordingly

## export_save_image directory
The image and container tar have been created manually by
```
docker export elastalert_container --output="elastalert_container-export.tar"
docker save elastalert_image > elastalert_image.tar
gzip *.tar
```

## Guff (ignore)
rancher-docker-compose.yml was imported into a new rancher 1.6 stack and works.

```
docker exec -it elastalert_compose sh // then gets a shell
cd <elastalert-container-build>
docker build . -t elastalert
docker run -d --privileged=true -v /home/anthony/git_workspaces/elastalert/docker_config_volume:/opt/config elastalert
docker run -it --privileged=true -v /home/anthony/git_workspaces/elastalert/docker_config_volume:/opt/config elastalert
docker run -it -v /home/anthony/git_workspaces/elastalert/docker_config_volume:/opt/config python:3.6-alpine ls -lR /opt
```

## Existing implementation
q) does elastalert.py exists in this directory?
python /opt/elastalert/elastalert/elastalert.py --config /opt/config/elastalert_config.yaml --verbose

Env vars
ELASTICSEARCH_HOST - kibana-1.sol  ? add to args
ELASTICSEARCH_PORT - 9200          ? add to args
ELASTALERT_CONFIG  - /opt/config/elastalert_config.yaml

Volumes
/docker/elastalert/rules         - /opt/rules
/docker/elastalert/logs          - /opt/logs
/docker/elastalert/config        - /opt/config
/docker/elastalert/scripts       - /opt/scripts
/docker/elastalert/expired-rules - /opt/expired-rules

rules folder is /opt/rules
